# Settings
import os

#GAME SETTINGS
FPS = 30
DISPLAY_WIDTH = 1600
DISPLAY_HEIGHT = 900
TITLE = 'Outlaw'
FONT_NAME = 'Marston-Bold.ttf'
WAIT_TIME_RANGE = (5,10) # in seconds
DEBUG_MODE = False

#BANDIT LEVELS
BANDIT_DIFFICULTY = 2
DIFFUCULTY_MODIFIER = 0.25

#COLORS
WHITE = (255,255,255)
BLACK = (0,0,0)

#ANIMATION LENGTH
SHOOT_ANIM_LENGTH = 42
DEATH_ANIM_LENGTH = 75

#ASSETS & FOLDERS
game_folder = os.path.dirname(__file__)
asset_folder = os.path.join(game_folder, 'assets')
img_folder = os.path.join(asset_folder,'imgs')
anims_folder = os.path.join(img_folder,'anims')
sheriff_anims_folder = os.path.join(anims_folder, 'sheriff')
bandit_anims_folder = os.path.join(anims_folder, 'bandit')
sound_folder = os.path.join(asset_folder,'sound')
font_folder = os.path.join(asset_folder,'font')