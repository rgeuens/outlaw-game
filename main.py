# High Noon Game
import random
import time
import os
import pygame as pg
from settings import *
from sprites import *

class Game():
	def __init__(self):
		# Initialize game
		# This part sets up everything we need to run the game properly
		# It initializes pygame, the mixer, sets the screen size,...
		# Different gamestates are:
		# - Loading
		# - Startscreen
		# - Waiting for duel
		# - Duelling
		# - Player won
		# - Game Over
		os.environ['SDL_VIDEO_CENTERED'] = '1'
		pg.init()
		pg.mixer.init()
		pg.display.set_caption(TITLE)
		self.screen = pg.display.set_mode((DISPLAY_WIDTH,DISPLAY_HEIGHT))
		self.font_name = pg.font.Font(os.path.join(font_folder,FONT_NAME),16)
		self.running = True
		self.gamestate = 'Startscreen'
		self.time_until_duel = 0
		self.time_until_death = 0
		self.game_level = 0
		self.key_down = False # DEBUG VAR

		# Sprite Setup
		self.all_sprites = pg.sprite.Group() # Create sprite group
		self.all_clouds = pg.sprite.Group() # Create cloud sprite group
		self.all_characters = pg.sprite.Group()

		g_background = Background() # Create background
		self.all_sprites.add(g_background) # Add background to group first

		self.cloud = Cloud('cloud_1')
		self.all_sprites.add(self.cloud)

		self.player = Character(400,450,'sheriff') # Create new instances of characters
		self.bandit = Character(DISPLAY_WIDTH - 400, 450, 'bandit') # Create Bandit

		self.all_sprites.add(self.player) # Add player and bandit to group third  to keep them above everything
		self.all_sprites.add(self.bandit)
		self.all_characters.add(self.bandit)
		self.all_characters.add(self.player)

		#Sound setup
		self.duel_suspense = pg.mixer.Sound(os.path.join(sound_folder,'duel_suspense.wav'))
		self.bg_sound = pg.mixer.Sound(os.path.join(sound_folder,'background_wind.wav'))
		pg.mixer.Channel(0).play(self.bg_sound)
		pg.mixer.Channel(1).play(self.duel_suspense)

		self.clock = pg.time.Clock()

	def new(self):
		# New game
		# Every time you start a new game, this runs
		self.all_sprites.draw(self.screen)

		self.run()

	def run(self):
		# Game loop
		self.playing = True
		while self.playing:
			self.clock.tick(FPS)
			self.events()
			self.update()
			self.draw()

	def events(self):
		# Game loop - Update
		# Put the gun draw event in here
		self.allow_key_press = True
		for event in pg.event.get():
			if event.type == pg.QUIT:
				self.playing = False
				self.running = False
			

			# If we're in the duelling phase of the game, check for keypresses and see if the spacebar is pushed
			# If it's pushed that means the player drew his gun at the right time, winning the game
			if event.type == pg.KEYDOWN and event.key == pg.K_SPACE:
				if self.gamestate == 'Startscreen':
					self.gamestate = 'Waiting for duel'
					self.startduel()

				if self.gamestate == 'Duelling':
					self.player.drawGun()
					self.bandit.die()
					self.game_level += 1
					self.gamestate = 'Player won'
					self.allow_key_press = False
					# For some reason It only works if I don't allow a key to be pressed.
					# Otherwise it will just execute the player won state

				if self.gamestate == 'Player won' and self.allow_key_press == True:
					self.gamestate = 'Waiting for duel'
					self.startduel()
					#Every time you go up a level the reaction time decreases by 0.25 seconds

				if self.gamestate == 'Game over':
					self.game_level = 0
					self.startduel()
					self.gamestate = 'Waiting for duel'

			if event.type == pg.KEYUP:
				self.allow_key_press = True

	def update(self):
		# Game loop - Update
		t0 = time.time() # Current time
		t1 = self.time_until_duel # Duel start time ( current time + wait time)
		t2 = self.time_until_death # Time until bandit shoots ( current time + wait time + bandit reaction time)

			
		if t0 > t1 and self.gamestate == 'Waiting for duel': # if the current time is larger than the duel start time, start the duel
			self.gamestate = 'Duelling'
		if t0 > t2 and self.gamestate == 'Duelling' and self.bandit.alive == True: # If the current time is larger than the bandit's reaction time, let the bandit shoot
			self.bandit.drawGun()
			self.player.die()
			self.gamestate = 'Game over'

		self.all_sprites.update()


	def draw(self):
		# Game loop - Draw
		self.all_sprites.draw(self.screen)

		if self.gamestate != 'Startscreen':
			self.draw_text('SCORE: '+str(self.game_level), 36, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 5/100)

		if self.gamestate == 'Startscreen':
			self.draw_text('OUTLAW', 120, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 25/100)
			self.draw_text('Press space to start', 48, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 85/100)

		if self.gamestate == 'Waiting for duel':
			self.draw_text('Get Ready . . .', 48, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 85/100 )

		if self.gamestate == 'Duelling':
			self.draw_text('DRAW!', 48, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 85/100 )
			self.draw_text('Press space to draw your gun', 36, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 90/100)

		if self.gamestate == 'Player won':
			self.draw_text('You won!', 48, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 85/100 )
			self.draw_text('Press space to start next level', 36, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 90/100)

		if self.gamestate == 'Game over':
			self.draw_text('Too late!', 48, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 85/100 )
			self.draw_text('Press space to restart', 36, WHITE, DISPLAY_WIDTH / 2, DISPLAY_HEIGHT * 90/100)

		if DEBUG_MODE == True:
			self.draw_text('Gamestate: ' + self.gamestate, 24, WHITE, DISPLAY_WIDTH * 10/100 , DISPLAY_HEIGHT * 5/100 )
			self.draw_text('Player idle?: ' + str(self.player.idle), 24, WHITE, DISPLAY_WIDTH * 10/100 , DISPLAY_HEIGHT * 10/100 )
			self.draw_text('Player shooting?: ' + str(self.player.shooting), 24, WHITE, DISPLAY_WIDTH * 10/100 , DISPLAY_HEIGHT * 15/100 )
			self.draw_text('Reaction time: ' + str(round(self.time_until_death - self.time_until_duel,3)), 24, WHITE, DISPLAY_WIDTH * 10/100 , DISPLAY_HEIGHT * 20/100 )
			self.draw_text('Anim Index: ' + str(self.player.anim_index), 24, WHITE, DISPLAY_WIDTH * 10/100 , DISPLAY_HEIGHT * 25/100 )
			self.draw_text('event type: ' + str(self.key_down), 24, WHITE, DISPLAY_WIDTH * 10/100 , DISPLAY_HEIGHT * 30/100 )


		pg.display.flip()

	def startduel(self):
		self.player.reset()
		self.bandit.reset()
		self.bandit_reaction_time = BANDIT_DIFFICULTY - self.game_level * DIFFUCULTY_MODIFIER
		self.time_until_duel = time.time() + random.randint(WAIT_TIME_RANGE[0],WAIT_TIME_RANGE[1]) # take the current time and add a random integer
		self.time_until_death = self.time_until_duel + self.bandit_reaction_time # Bandit reaction time is currently manual
		

		# Sound Setup
		self.duel_suspense.fadeout(1500) #fade out title music

	def draw_text(self, text, size, color, x, y):
		font = pg.font.Font(os.path.join(font_folder,FONT_NAME),size)
		# text_surface = text / anti aliasing / color
		text_surface = font.render(text, True, color)
		text_rect = text_surface.get_rect()
		text_rect.midtop = (x,y)
		self.screen.blit(text_surface, text_rect)

g = Game()
while g.running:
	g.new()

pg.quit()
quit()

