# Sprite Classes
import os
import random
import pygame as pg
from settings import *
vec = pg.math.Vector2

class Character(pg.sprite.Sprite):
	def __init__(self, x, y, characterType):
		pg.sprite.Sprite.__init__(self)
		self.character = characterType
		self.loadImages(self.character)
		self.alive = True
		self.idle = True
		self.shooting = False
		self.dying = False
		self.image = self.idle_anim[0]
		self.rect = self.image.get_rect()
		self.rect.center = (x,y) #Position the player around one fourth of the screen to the left and about two thirds from the top
		self.anim_index = 0

	def loadImages(self, character):
		characterFolder = os.path.join(anims_folder, character)
		self.idle_anim = []
		self.idle_anim.append(pg.image.load(os.path.join(characterFolder,'shoot_0.png')).convert_alpha())

		self.shoot_anim = []
		for frame in range(SHOOT_ANIM_LENGTH):
			self.shoot_anim.append(pg.image.load(os.path.join(characterFolder,'shoot_'+str(frame)+'.png')).convert_alpha())

		self.death_anim = []
		for frame in range(DEATH_ANIM_LENGTH):
			self.death_anim.append(pg.image.load(os.path.join(characterFolder,'death_'+str(frame)+'.png')).convert_alpha())

	def update(self):
		if self.idle == True:
			self.image = self.idle_anim[0]

		if self.shooting == True:
			self.idle = False
			self.runAnimation(self.shoot_anim)

		if self.dying == True:
			self.idle = False
			self.runAnimation(self.death_anim)

		if self.alive == False:
			self.idle = False
			self.shooting = False
			self.image = self.death_anim[74]

	def runAnimation(self, animation):
		self.anim_index += 1
		if self.anim_index > len(animation) - 1:
			self.image = animation[0]
			self.idle = True
			self.anim_index = 0

			if self.shooting == True:
				self.shooting = False

			if self.dying == True:
				self.shooting = False
				self.dying = False
				self.alive = False

		self.image = animation[self.anim_index]

	def drawGun(self):
		self.shooting = True
		self.idle = False
		self.fire_sound = pg.mixer.Sound(os.path.join(sound_folder,'fire-1.wav'))
		self.fire_sound.play()

	def die(self):
		self.dying = True

	def reset(self):
		self.image = self.idle_anim[0]
		self.idle = True
		self.shooting = False
		self.alive = True
		self.dying = False
		self.anim_index = 0


class Background(pg.sprite.Sprite):
	def __init__(self):
		pg.sprite.Sprite.__init__(self)
		self.image = pg.image.load(os.path.join(img_folder,'duel_background.png')).convert()
		self.rect = self.image.get_rect()
		self.rect.center = (DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2)



class Cloud(pg.sprite.Sprite):
	def __init__(self, image):
		pg.sprite.Sprite.__init__(self)
		self.image = pg.image.load(os.path.join(img_folder,image +'.png')).convert_alpha()

		#Pick a random position to start with - float
		self.x = random.randint(0, DISPLAY_WIDTH)
		self.y = 300

		# Get the position of the cloud
		self.pos = vec(self.x,self.y)
		self.rect = self.image.get_rect()
		self.rect.center = (self.x, self.y)
		self.speed = random.uniform(0.05, 0.75)

	def update(self):
		self.pos += (self.speed, 0) # Move the cloud to the right by a random amount between 0.2 and 1 (self.speed) - X and y coordinates
		if self.pos.x > (DISPLAY_WIDTH + self.rect.width / 2): # If the cloud moves past the screen width, move it to the left side of the screen minus half of the cloud size.
			self.pos.x = (0 - self.rect.width / 2) # This makes sure the right side of the cloud doesn't just pop up on screen
			self.pos.y = 300

		self.rect.center = self.pos